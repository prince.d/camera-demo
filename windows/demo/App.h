#pragma once

#include "App.xaml.g.h"



namespace winrt::demo::implementation
{
    struct App : AppT<App>
    {
        App() noexcept;
    };
} // namespace winrt::demo::implementation


