import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TextInput, Button, Platform, Image, TouchableOpacity } from 'react-native';
import { RNCamera } from 'react-native-camera';

const App = () => {
  const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const regexPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState(true);
  const [passwordError, setPasswordError] = useState(true);
  const [uri, setUri] = useState(null);

  const checkValidation = (inputEmail, inputPassword) => {
    const isEmailValid = regexEmail.test(String(inputEmail).toLowerCase());
    const isPasswordValid = regexPassword.test(String(inputPassword))

    setEmailError(isEmailValid);
    setPasswordError(isPasswordValid);
    return isEmailValid && isPasswordValid;
  };

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true , target: 3};
      const data = await this.camera.takePictureAsync(options);
      
      setUri({ uri: Platform.select({
        android: data.uri,
        windows: data.path
      }) });
    }
  };

  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>LOGIN</Text>
      <View style={styles.inputContainer}>
        <TextInput
          autoCorrect={false}
          autoCapitalize="none"
          spellCheck={false}
          style={styles.textInput}
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
          value={email}
        />
        {
          emailError
            ? null
            : <Text style={styles.validationStyle}>Enter valid email</Text>
        }
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          secureTextEntry
          style={styles.textInput}
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          value={password}
        />
        {
          passwordError
            ? null
            : <Text style={styles.validationStyle}>Enter valid password</Text>
        }
      </View>
      <Button
        title={'Login'}
        onPress={() => {
          checkValidation(email, password);
        }}
      /> */}
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          width: '100%',
        }}
      />
      <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
        <TouchableOpacity onPress={takePicture.bind(this)}>
          <Text style={{ fontSize: 14 }}> SNAP </Text>
        </TouchableOpacity>
      </View>
      
      <Image source={uri} style={{ width: 100, height: 100 }} />
    </View >
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    ...Platform.select({
      android: {
        paddingHorizontal: 36
      },
      windows: {
        paddingHorizontal: 100
      }
    })
  },
  title: {
    fontSize: 30,
    marginBottom: 80,
    fontWeight: 'bold',
    alignSelf: 'center'
  },

  textInput: {
    fontSize: 18,
    height: 40,
    ...Platform.select({
      android: {
        borderBottomColor: "grey",
        borderBottomWidth: 1
      }
    })
  },

  inputContainer: {
    marginBottom: 30
  },

  validationStyle: {
    fontSize: 14,
    color: 'red'
  }
});

export default App;
